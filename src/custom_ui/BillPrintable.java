/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package custom_ui;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class BillPrintable implements Printable {

    private JPanel frame;
    private ArrayList<String> itemName = new ArrayList<>();
    private ArrayList<String> quantity = new ArrayList<>();
    private ArrayList<String> itemPrice = new ArrayList<>();

    public BillPrintable(JPanel frame, ArrayList<String> itemName, ArrayList<String> quantity, ArrayList<String> itemPrice) {
        this.frame = frame;
        this.itemName = itemName;
        this.quantity = quantity;
        this.itemPrice = itemPrice;
    }

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
            int r = itemName.size();
            ImageIcon icon = new ImageIcon(getClass().getResource("Images/logorestaurant.png"));
            int result = NO_SUCH_PAGE;
            if (pageIndex == 0) {

                Graphics2D g2d = (Graphics2D) graphics;
                double width = pageFormat.getImageableWidth();
                g2d.translate((int) pageFormat.getImageableX(), (int) pageFormat.getImageableY());
                try {
                    int y = 20;
                    int yShift = 10;
                    int headerRectHeight = 15;

                    g2d.setFont(new Font("Monospaced", Font.PLAIN, 9));
                    g2d.drawImage(icon.getImage(), 50, 20, 90, 30, frame);
                    y += yShift + 30;
                    g2d.drawString("-------------------------------------", 12, y);
                    y += yShift;
                    g2d.drawString("         Imagine Restro        ", 12, y);
                    y += yShift;
                    g2d.drawString("   Nagarjun 5 ", 12, y);
                    y += yShift;
                    g2d.drawString("   Swyambhu, Chakkdol ", 12, y);
                    y += yShift;
                    g2d.drawString("   www.facebook.com/imagine ", 12, y);
                    y += yShift;
                    g2d.drawString("   www.facebook.com/imagine ", 12, y);
                    y += yShift;
                    g2d.drawString("        +977-9846765534      ", 12, y);
                    y += yShift;
                    g2d.drawString("-------------------------------------", 12, y);
                    y += headerRectHeight;

                    g2d.drawString(" Item Name                  Price   ", 10, y);
                    y += yShift;
                    g2d.drawString("-------------------------------------", 10, y);
                    y += headerRectHeight;

                    for (int s = 0; s < r; s++) {
                        g2d.drawString(" " + itemName.get(s) + "                            ", 10, y);
                        y += yShift;
                        g2d.drawString("      " + quantity.get(s) + " * " + itemPrice.get(s), 10, y);
                        g2d.drawString(getSubTotal(quantity.get(s), itemPrice.get(s)), 160, y);
                        y += yShift;

                    }

                    g2d.drawString("-------------------------------------", 10, y);
                    y += yShift;
                    g2d.drawString(" Total amount:               " + getTotalAmount() + "   ", 10, y);
                    y += yShift;
//                g2d.drawString("-------------------------------------", 10, y);
//                y += yShift;
//                g2d.drawString(" Cash      :                 " + txtcash.getText() + "   ", 10, y);
//                y += yShift;
//                g2d.drawString("-------------------------------------", 10, y);
//                y += yShift;
//                g2d.drawString(" Balance   :                 " + txtbalance.getText() + "   ", 10, y);
//                y += yShift;

                    g2d.drawString("*************************************", 10, y);
                    y += yShift;
                    g2d.drawString("       THANK YOU COME AGAIN            ", 10, y);
                    y += yShift;
                    g2d.drawString("*************************************", 10, y);
                    y += yShift;
                    g2d.drawString("   CONTACT: contact@codeguid.com       ", 10, y);
                    y += yShift;

                } catch (Exception e) {
                    e.printStackTrace();
                }

                result = PAGE_EXISTS;
            }
            return result;
    }

    private String getSubTotal(String qty, String rt) {
        int quantity = Integer.parseInt(qty);
        int rate = Integer.parseInt(rt);
        return String.valueOf(quantity * rate);
    }

    private String getTotalAmount() {
        int total = 0;
        for (int i = 0; i < quantity.size(); i++) {
            total = total + (Integer.parseInt(quantity.get(i)) * Integer.parseInt(itemPrice.get(i)));
        }
        return String.valueOf(total);
    }
}
