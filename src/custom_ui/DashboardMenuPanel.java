/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package custom_ui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 *
 * @author saile
 */
public class DashboardMenuPanel extends JPanel {
    

    public DashboardMenuPanel() {
        setOpaque(true);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);  
        ImageIcon img = new ImageIcon(getClass().getResource("/Images/dashboard.png")); //full path of image
        Image img2 = img.getImage();
        g.drawImage(img2, 0, 0, this.getWidth(), this.getHeight(), null);
    }
}
