package frames;

import dbhandler.DBHandler;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import panels.BillingPanel;
import panels.DashboardPanel;
import panels.FoodPanel;
import panels.HistoryPanel;
import panels.SelectTablePanel;
import panels.TablemanagementPanel;

public class Dashboard extends javax.swing.JFrame {
    
    public Dashboard() {
        initComponents();
        setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);
        showForm(new DashboardPanel(), "Dashboard");
    }
    
    private void selected(JLabel label) {
        label.setFont(new Font("Tahoma", Font.PLAIN, 16));
        label.setForeground(new Color(0, 51, 102));
    }
    
    private void disSelect(JLabel label) {
        label.setFont(new Font("Tahoma", Font.PLAIN, 11));
        label.setForeground(new Color(0, 0, 0));
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        imageAvatar1 = new custom_ui.ImageAvatar();
        jPanel3 = new javax.swing.JPanel();
        panelMain = new javax.swing.JPanel();
        dashboardMenuPanel1 = new custom_ui.DashboardMenuPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        mnuHome = new javax.swing.JLabel();
        mnuFood = new javax.swing.JLabel();
        mnuTableManagement = new javax.swing.JLabel();
        mnuBilling = new javax.swing.JLabel();
        mnuHistory = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        lblTitle = new javax.swing.JLabel();

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(248, 248, 248));
        setState(2);

        panelMain.setBackground(new java.awt.Color(248, 248, 248));
        panelMain.setLayout(new java.awt.BorderLayout());

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 51, 102));
        jLabel1.setText("Imagine Restro & Bar");

        jLabel2.setForeground(new java.awt.Color(139, 137, 136));
        jLabel2.setText("Swyambhu, Chhakdol");

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/logo.png"))); // NOI18N

        mnuHome.setBackground(new java.awt.Color(231, 234, 238));
        mnuHome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        mnuHome.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/home.png"))); // NOI18N
        mnuHome.setText("     Home");
        mnuHome.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                mnuHomeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                mnuHomeMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                mnuHomeMousePressed(evt);
            }
        });

        mnuFood.setBackground(new java.awt.Color(231, 234, 238));
        mnuFood.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/food.png"))); // NOI18N
        mnuFood.setText("     Food");
        mnuFood.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                mnuFoodMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                mnuFoodMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                mnuFoodMousePressed(evt);
            }
        });

        mnuTableManagement.setBackground(new java.awt.Color(231, 234, 238));
        mnuTableManagement.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/table.png"))); // NOI18N
        mnuTableManagement.setText("     Table Management");
        mnuTableManagement.setPreferredSize(new java.awt.Dimension(65, 24));
        mnuTableManagement.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                mnuTableManagementMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                mnuTableManagementMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                mnuTableManagementMousePressed(evt);
            }
        });

        mnuBilling.setBackground(new java.awt.Color(231, 234, 238));
        mnuBilling.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/billing.png"))); // NOI18N
        mnuBilling.setText("     Billing");
        mnuBilling.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                mnuBillingMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                mnuBillingMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                mnuBillingMousePressed(evt);
            }
        });

        mnuHistory.setBackground(new java.awt.Color(231, 234, 238));
        mnuHistory.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/parchment.png"))); // NOI18N
        mnuHistory.setText("    History");
        mnuHistory.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                mnuHistoryMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                mnuHistoryMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                mnuHistoryMousePressed(evt);
            }
        });

        javax.swing.GroupLayout dashboardMenuPanel1Layout = new javax.swing.GroupLayout(dashboardMenuPanel1);
        dashboardMenuPanel1.setLayout(dashboardMenuPanel1Layout);
        dashboardMenuPanel1Layout.setHorizontalGroup(
            dashboardMenuPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dashboardMenuPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dashboardMenuPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mnuTableManagement, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mnuHome, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(dashboardMenuPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addGroup(dashboardMenuPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addGap(0, 29, Short.MAX_VALUE))
                    .addComponent(mnuFood, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mnuBilling, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mnuHistory, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
        dashboardMenuPanel1Layout.setVerticalGroup(
            dashboardMenuPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dashboardMenuPanel1Layout.createSequentialGroup()
                .addGroup(dashboardMenuPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(dashboardMenuPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(dashboardMenuPanel1Layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                        .addComponent(jLabel2)))
                .addGap(51, 51, 51)
                .addComponent(mnuHome, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mnuFood, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mnuTableManagement, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mnuBilling, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mnuHistory, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(273, Short.MAX_VALUE))
        );

        jPanel1.setBackground(new java.awt.Color(248, 248, 248));

        lblTitle.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        lblTitle.setForeground(new java.awt.Color(0, 51, 102));
        lblTitle.setText("Home");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(lblTitle)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(30, Short.MAX_VALUE)
                .addComponent(lblTitle))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(dashboardMenuPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelMain, javax.swing.GroupLayout.DEFAULT_SIZE, 689, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(dashboardMenuPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(panelMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mnuHistoryMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mnuHistoryMouseEntered
        selected(mnuHistory);
    }//GEN-LAST:event_mnuHistoryMouseEntered

    private void mnuHistoryMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mnuHistoryMouseExited
        disSelect(mnuHistory);
    }//GEN-LAST:event_mnuHistoryMouseExited

    private void mnuHistoryMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mnuHistoryMousePressed
        showForm(new HistoryPanel(), "History");
    }//GEN-LAST:event_mnuHistoryMousePressed

    private void mnuHomeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mnuHomeMouseEntered
        selected(mnuHome);
    }//GEN-LAST:event_mnuHomeMouseEntered

    private void mnuHomeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mnuHomeMouseExited
        disSelect(mnuHome);
    }//GEN-LAST:event_mnuHomeMouseExited

    private void mnuHomeMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mnuHomeMousePressed
        showForm(new DashboardPanel(), "Dashboard");
    }//GEN-LAST:event_mnuHomeMousePressed

    private void mnuFoodMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mnuFoodMouseEntered
        selected(mnuFood);
    }//GEN-LAST:event_mnuFoodMouseEntered

    private void mnuFoodMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mnuFoodMouseExited
        disSelect(mnuFood);
    }//GEN-LAST:event_mnuFoodMouseExited

    private void mnuFoodMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mnuFoodMousePressed
        showForm(new FoodPanel(), "Food");
    }//GEN-LAST:event_mnuFoodMousePressed

    private void mnuTableManagementMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mnuTableManagementMouseEntered
        selected(mnuTableManagement);
    }//GEN-LAST:event_mnuTableManagementMouseEntered

    private void mnuTableManagementMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mnuTableManagementMouseExited
        disSelect(mnuTableManagement);
    }//GEN-LAST:event_mnuTableManagementMouseExited

    private void mnuTableManagementMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mnuTableManagementMousePressed
        showForm(new TablemanagementPanel(), "Table Management");
    }//GEN-LAST:event_mnuTableManagementMousePressed

    private void mnuBillingMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mnuBillingMouseEntered
        selected(mnuBilling);
    }//GEN-LAST:event_mnuBillingMouseEntered

    private void mnuBillingMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mnuBillingMouseExited
        disSelect(mnuBilling);
    }//GEN-LAST:event_mnuBillingMouseExited

    private void mnuBillingMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mnuBillingMousePressed
        showForm(new SelectTablePanel(this), "Billing");
    }//GEN-LAST:event_mnuBillingMousePressed
    
    public void showBillingForm(int id) {
        showForm(new BillingPanel(id), "Billing");
    }

    private void showForm(JPanel panel, String title) {
        panelMain.removeAll();
        panelMain.add(panel);
        panelMain.repaint();
        panelMain.revalidate();
        lblTitle.setText(title);
    }

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Dashboard.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        java.awt.EventQueue.invokeLater(() -> {
            new Dashboard().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private custom_ui.DashboardMenuPanel dashboardMenuPanel1;
    private custom_ui.ImageAvatar imageAvatar1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JLabel mnuBilling;
    private javax.swing.JLabel mnuFood;
    private javax.swing.JLabel mnuHistory;
    private javax.swing.JLabel mnuHome;
    private javax.swing.JLabel mnuTableManagement;
    private javax.swing.JPanel panelMain;
    // End of variables declaration//GEN-END:variables
}
