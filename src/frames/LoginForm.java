/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frames;

import dbhandler.DBHandler;
import global.Utils;

/**
 *
 * @author samitalimbu
 */
public class LoginForm extends javax.swing.JFrame {

    /**
     * Creates new form MainFrame
     */
    public LoginForm() {
        initComponents();
        setLocationRelativeTo(null);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelGradiente1 = new custom_ui.PanelGradient();
        jLabel2 = new javax.swing.JLabel();
        etUsername = new custom_ui.MyTextField();
        etPassword = new custom_ui.MyTextField();
        btnLogin = new custom_ui.ButtonGradient();
        jLabel3 = new javax.swing.JLabel();
        chkTerms = new javax.swing.JCheckBox();
        imageAvatar7 = new custom_ui.ImageAvatar();
        imageAvatar8 = new custom_ui.ImageAvatar();
        imageAvatar9 = new custom_ui.ImageAvatar();
        imageAvatar10 = new custom_ui.ImageAvatar();
        imageAvatar11 = new custom_ui.ImageAvatar();
        imageAvatar12 = new custom_ui.ImageAvatar();
        imageAvatar1 = new custom_ui.ImageAvatar();
        imageAvatar2 = new custom_ui.ImageAvatar();
        imageAvatar3 = new custom_ui.ImageAvatar();
        circle1 = new custom_ui.Circle();
        circle2 = new custom_ui.Circle();
        circle3 = new custom_ui.Circle();
        circle4 = new custom_ui.Circle();
        circle5 = new custom_ui.Circle();
        jLabel1 = new javax.swing.JLabel();
        lblSignup = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBounds(new java.awt.Rectangle(0, 0, 0, 0));

        panelGradiente1.setColorPrimario(new java.awt.Color(197, 230, 228));
        panelGradiente1.setColorSecundario(new java.awt.Color(197, 201, 236));

        jLabel2.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Sign In");
        panelGradiente1.add(jLabel2);
        jLabel2.setBounds(140, 240, 52, 17);

        etUsername.setForeground(new java.awt.Color(51, 51, 51));
        panelGradiente1.add(etUsername);
        etUsername.setBounds(30, 280, 300, 40);

        etPassword.setForeground(new java.awt.Color(51, 51, 51));
        panelGradiente1.add(etPassword);
        etPassword.setBounds(30, 330, 300, 40);

        btnLogin.setText("Login");
        btnLogin.setColor1(new java.awt.Color(139, 199, 236));
        btnLogin.setColor2(new java.awt.Color(55, 131, 210));
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });
        panelGradiente1.add(btnLogin);
        btnLogin.setBounds(30, 420, 300, 36);

        jLabel3.setFont(new java.awt.Font("Lucida Grande", 1, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Welcome!");
        panelGradiente1.add(jLabel3);
        jLabel3.setBounds(120, 170, 130, 30);

        chkTerms.setFont(new java.awt.Font("Lucida Grande", 0, 10)); // NOI18N
        chkTerms.setForeground(new java.awt.Color(255, 255, 255));
        chkTerms.setText(" I Accept terms and conditions & and privacy policy");
        panelGradiente1.add(chkTerms);
        chkTerms.setBounds(30, 380, 290, 23);

        imageAvatar7.setBorderSize(0);

        imageAvatar8.setBorderSize(0);
        imageAvatar8.setImage(new javax.swing.ImageIcon("/Users/samitalimbu/Downloads/google.png")); // NOI18N

        imageAvatar9.setBorderSize(0);
        imageAvatar9.setImage(new javax.swing.ImageIcon("/Users/samitalimbu/Downloads/google.png")); // NOI18N
        imageAvatar8.add(imageAvatar9);
        imageAvatar9.setBounds(50, 350, 40, 40);

        imageAvatar7.add(imageAvatar8);
        imageAvatar8.setBounds(50, 350, 40, 40);

        imageAvatar10.setBorderSize(0);

        imageAvatar11.setBorderSize(0);
        imageAvatar11.setImage(new javax.swing.ImageIcon("/Users/samitalimbu/Downloads/google.png")); // NOI18N

        imageAvatar12.setBorderSize(0);
        imageAvatar12.setImage(new javax.swing.ImageIcon("/Users/samitalimbu/Downloads/google.png")); // NOI18N
        imageAvatar11.add(imageAvatar12);
        imageAvatar12.setBounds(50, 350, 40, 40);

        imageAvatar10.add(imageAvatar11);
        imageAvatar11.setBounds(50, 350, 40, 40);

        imageAvatar7.add(imageAvatar10);
        imageAvatar10.setBounds(140, 350, 40, 40);

        panelGradiente1.add(imageAvatar7);
        imageAvatar7.setBounds(170, 390, 40, 40);

        imageAvatar1.setBorderSize(0);

        imageAvatar2.setBorderSize(0);
        imageAvatar2.setImage(new javax.swing.ImageIcon("/Users/samitalimbu/Downloads/google.png")); // NOI18N

        imageAvatar3.setBorderSize(0);
        imageAvatar3.setImage(new javax.swing.ImageIcon("/Users/samitalimbu/Downloads/google.png")); // NOI18N
        imageAvatar2.add(imageAvatar3);
        imageAvatar3.setBounds(50, 350, 40, 40);

        imageAvatar1.add(imageAvatar2);
        imageAvatar2.setBounds(50, 350, 40, 40);

        panelGradiente1.add(imageAvatar1);
        imageAvatar1.setBounds(250, 390, 40, 40);

        circle1.setBackground(new java.awt.Color(197, 230, 238));
        panelGradiente1.add(circle1);
        circle1.setBounds(260, 530, 110, 110);

        circle2.setBackground(new java.awt.Color(197, 230, 238));
        panelGradiente1.add(circle2);
        circle2.setBounds(260, -10, 120, 120);

        circle3.setBackground(new java.awt.Color(197, 230, 238));
        panelGradiente1.add(circle3);
        circle3.setBounds(-10, 520, 120, 120);

        circle4.setBackground(new java.awt.Color(197, 230, 238));
        panelGradiente1.add(circle4);
        circle4.setBounds(40, 40, 120, 120);

        circle5.setBackground(new java.awt.Color(197, 230, 238));
        panelGradiente1.add(circle5);
        circle5.setBounds(50, 550, 90, 90);

        jLabel1.setText("Don't have account ?");
        panelGradiente1.add(jLabel1);
        jLabel1.setBounds(40, 470, 130, 16);

        lblSignup.setForeground(new java.awt.Color(0, 102, 255));
        lblSignup.setText("Sign Up");
        lblSignup.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblSignupMouseClicked(evt);
            }
        });
        panelGradiente1.add(lblSignup);
        lblSignup.setBounds(180, 470, 50, 16);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelGradiente1, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelGradiente1, javax.swing.GroupLayout.DEFAULT_SIZE, 615, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed
        if (!validateForm()) {
            return;
        }
        String username = etUsername.getText();
        String password = etPassword.getText();
        
        DBHandler db = new DBHandler();
        
        if (db.loginUser(username, password)) {
            Utils.showSuccessMessage(this, "User logged in successfully.");
            dispose();
            new Dashboard().setVisible(true);
        } else {
            Utils.showErrorMessage(this, "Username and password doesnot match.");
        }
        
    }//GEN-LAST:event_btnLoginActionPerformed

    private void lblSignupMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblSignupMouseClicked
        dispose();
        new RegisterForm().setVisible(true);
    }//GEN-LAST:event_lblSignupMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LoginForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LoginForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LoginForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LoginForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LoginForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private custom_ui.ButtonGradient btnLogin;
    private javax.swing.JCheckBox chkTerms;
    private custom_ui.Circle circle1;
    private custom_ui.Circle circle2;
    private custom_ui.Circle circle3;
    private custom_ui.Circle circle4;
    private custom_ui.Circle circle5;
    private custom_ui.MyTextField etPassword;
    private custom_ui.MyTextField etUsername;
    private custom_ui.ImageAvatar imageAvatar1;
    private custom_ui.ImageAvatar imageAvatar10;
    private custom_ui.ImageAvatar imageAvatar11;
    private custom_ui.ImageAvatar imageAvatar12;
    private custom_ui.ImageAvatar imageAvatar2;
    private custom_ui.ImageAvatar imageAvatar3;
    private custom_ui.ImageAvatar imageAvatar7;
    private custom_ui.ImageAvatar imageAvatar8;
    private custom_ui.ImageAvatar imageAvatar9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel lblSignup;
    private custom_ui.PanelGradient panelGradiente1;
    // End of variables declaration//GEN-END:variables

    private boolean validateForm() {
        if (etUsername.getText().equals("")) {
            Utils.showErrorMessage(this, "Please enter username");
            return false;
        } else if (etPassword.getText().equals("")) {
            Utils.showErrorMessage(this, "Please enter password");            
            return false;            
        } else if (!chkTerms.isSelected()) {
            Utils.showErrorMessage(this, "Please accept our terms and conditions");
            return false;
        }
        return true;
        
    }
}
