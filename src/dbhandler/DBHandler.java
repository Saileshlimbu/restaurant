package dbhandler;

import global.Utils;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.table.DefaultTableModel;
import models.BillingModel;
import models.FinalBillModel;
import models.FoodModel;
import models.TableModel;

public class DBHandler {

    private Connection con;

    public void getConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/restaurant", "root", "12345678");
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e);
        }
    }

    public DBHandler() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/restaurant", "root", "12345678");
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e);
        }
    }

    public boolean registerUser(String username, String password, String email) {
        getConnection();

        try {
            PreparedStatement ps = con.prepareStatement("insert into users (username, password, email) values (?,?,?)");
            ps.setString(1, username);
            ps.setString(2, password);
            ps.setString(3, email);
            return ps.executeUpdate() == 1;
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public boolean loginUser(String username, String password) {
        getConnection();
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from users where username='" + username + "' and password='" + password + "'");
            return rs.next();
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean addFoodItem(FoodModel food) {
        getConnection();
        try {
            PreparedStatement pst = con.prepareStatement("insert into foods(name, category, price) values(?,?,?)");
            pst.setString(1, food.getName());
            pst.setString(2, food.getCategory());
            pst.setInt(3, food.getPrice());
            return pst.executeUpdate() == 1;
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean updateFoodItem(FoodModel food) {
        getConnection();
        try {
            PreparedStatement pst = con.prepareStatement("update foods set name=? , category=? , price=? where id=?");
            pst.setString(1, food.getName());
            pst.setString(2, food.getCategory());
            pst.setInt(3, food.getPrice());
            pst.setInt(4, food.getId());
            return pst.executeUpdate() == 1;
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean deleteFood(int id) {
        getConnection();
        try {
            PreparedStatement pst = con.prepareStatement("delete from foods where id=?");
            pst.setInt(1, id);
            return pst.executeUpdate() == 1;
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public DefaultTableModel getFoodList() {
        getConnection();
        String[] column = new String[]{"ID", "Name", "Category", "Price"};
        DefaultTableModel model = new DefaultTableModel(column, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false; //To change body of generated methods, choose Tools | Templates.
            }

        };
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from foods");
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                String category = rs.getString(3);
                int price = rs.getInt(4);
                Vector vector = new Vector();
                vector.add(id);
                vector.add(name);
                vector.add(category);
                vector.add(price);
                model.addRow(vector);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return model;
    }

    public DefaultTableModel getFoodList(String sName) {
        getConnection();
        String[] column = new String[]{"ID", "Name", "Category", "Price"};
        DefaultTableModel model = new DefaultTableModel(column, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false; //To change body of generated methods, choose Tools | Templates.
            }

        };
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from foods where name like '%" + sName + "%'");
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                String category = rs.getString(3);
                int price = rs.getInt(4);
                Vector vector = new Vector();
                vector.add(id);
                vector.add(name);
                vector.add(category);
                vector.add(price);
                model.addRow(vector);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return model;
    }

    public boolean addTable(TableModel table) {
        getConnection();
        try {
            PreparedStatement pst = con.prepareStatement("insert into tables(name) values(?)");
            pst.setString(1, table.getName());
            return pst.executeUpdate() == 1;
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean updateTable(TableModel table) {
        getConnection();
        try {
            PreparedStatement pst = con.prepareStatement("update tables set name=? where id=?");
            pst.setString(1, table.getName());
            pst.setInt(2, table.getId());
            return pst.executeUpdate() == 1;
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean deleteTable(int id) {
        getConnection();
        try {
            PreparedStatement pst = con.prepareStatement("delete from tables where id=?");
            pst.setInt(1, id);
            return pst.executeUpdate() == 1;
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public DefaultListModel getTableList() {
        getConnection();
        DefaultListModel model = new DefaultListModel();
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from tables");
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                model.addElement(new TableModel(id, name));
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return model;
    }

    public DefaultListModel getTableNameList() {
        getConnection();
        DefaultListModel model = new DefaultListModel();
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from tables");
            while (rs.next()) {
                String name = rs.getString(2);
                model.addElement(name);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return model;
    }

    public String getTableName(int id) {
        getConnection();
        String name = "";
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select name from tables where id=" + id);
            while (rs.next()) {
                name = rs.getString(1);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return name;
    }

    public List<TableModel> getTableArrayList() {
        getConnection();
        List<TableModel> list = new ArrayList<>();
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from tables");
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                list.add(new TableModel(id, name));
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public boolean addBill(BillingModel model, int billNo) {
        getConnection();
        try {
            PreparedStatement pst = con.prepareStatement("insert into temp_bill(name, qty, price, total, table_id, billno) values(?,?,?,?,?,?)");
            pst.setString(1, model.getName());
            pst.setInt(2, model.getQty());
            pst.setInt(3, model.getPrice());
            pst.setInt(4, (model.getQty() * model.getPrice()));
            pst.setInt(5, model.getTable_id());
            pst.setInt(6, billNo);
            return pst.executeUpdate() == 1;
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean hasAlreadyBill(int table_id) {
        getConnection();
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from temp_bill where table_id=" + table_id);
            return rs.next();
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public int getBillNo(int table_id) {
        getConnection();
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select billno from temp_bill where table_id=" + table_id);
            rs.next();
            return rs.getInt(1);
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public boolean switchTable(int currentTableID, int nextTableID) {
        getConnection();
        if (isTableEmpty(nextTableID)) {
            try {
                PreparedStatement pst = con.prepareStatement("update temp_bill set table_id=? where table_id=?");
                pst.setInt(1, nextTableID);
                pst.setInt(2, currentTableID);
                return pst.executeUpdate() > 0;
            } catch (SQLException ex) {
                Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

    public boolean isTableEmpty(int tableID) {
        getConnection();
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from temp_bill where table_id=" + tableID);
            return !rs.next();
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    public List<BillingModel> getBill(int table_no) {
        getConnection();
        List<BillingModel> billList = new ArrayList<>();
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from temp_bill where table_id=" + table_no);
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                int qty = rs.getInt(3);
                int price = rs.getInt(4);
                int total = rs.getInt(5);
                BillingModel model = new BillingModel();
                model.setId(id);
                model.setName(name);
                model.setQty(qty);
                model.setPrice(price);
                model.setTotal(total);
                model.setTable_id(table_no);
                billList.add(model);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return billList;
    }

    public boolean deleteBillItem(int id) {
        getConnection();
        try {
            PreparedStatement pst = con.prepareStatement("delete from temp_bill where id=?");
            pst.setInt(1, id);
            return pst.executeUpdate() == 1;
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean updateBillItem(int id, int qty) {
        getConnection();
        try {
            PreparedStatement pst = con.prepareStatement("update temp_bill set qty=? where id=?");
            pst.setInt(1, qty);
            pst.setInt(2, id);
            return pst.executeUpdate() == 1;
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean payBill(FinalBillModel model) {
        getConnection();
        try {
            PreparedStatement pst = con.prepareStatement("insert into final_bill(billno, total, discount, grand_total, date_time) values(?,?,?,?,?)");
            pst.setInt(1, model.getBillNo());
            pst.setInt(2, model.getTotal());
            pst.setInt(3, model.getDiscount());
            pst.setInt(4, model.getGrandTotal());
            pst.setString(5, model.getDateTime());
            return pst.executeUpdate() == 1;
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public void saveToHistory(List<BillingModel> billList, int billNo) {
        getConnection();
        for (BillingModel model : billList) {
            try {
                PreparedStatement pst = con.prepareStatement("insert into bill_history(name, qty, total, price, table_id, billno) values(?,?,?,?,?,?)");
                pst.setString(1, model.getName());
                pst.setInt(2, model.getQty());
                pst.setInt(3, model.getTotal());
                pst.setInt(4, model.getPrice());
                pst.setInt(5, model.getTable_id());
                pst.setInt(6, billNo);
                pst.executeUpdate();
            } catch (SQLException ex) {
                Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        clearTable(billList.get(0).getTable_id());
    }

    public void clearTable(int tableID) {
        getConnection();
        try {
            PreparedStatement pst = con.prepareStatement("delete from temp_bill where table_id=?");
            pst.setInt(1, tableID);
            pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Final Billing
    public DefaultTableModel getFinalBillList() {
        getConnection();
        String[] column = new String[]{"Bill No", "Total", "Discount", "Grand Total", "Date Time"};
        DefaultTableModel model = new DefaultTableModel(column, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false; //To change body of generated methods, choose Tools | Templates.
            }
        };
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from final_bill");
            while (rs.next()) {
                int billNo = rs.getInt(1);
                int total = rs.getInt(2);
                int discount = rs.getInt(3);
                int grandTotal = rs.getInt(4);
                String dateTime = rs.getString(5);
                Vector vector = new Vector();
                vector.add(billNo);
                vector.add("Rs. " + total);
                vector.add("Rs. " + discount);
                vector.add("Rs. " + grandTotal);
                vector.add(dateTime);
                model.addRow(vector);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return model;
    }

    public DefaultTableModel searchFinalBill(String searchText) {
        getConnection();
        String[] column = new String[]{"Bill No", "Total", "Discount", "Grand Total", "Date Time"};
        DefaultTableModel model = new DefaultTableModel(column, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false; //To change body of generated methods, choose Tools | Templates.
            }

        };
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from final_bill where concat(billno,date_time) like'%" + searchText + "%'");
            while (rs.next()) {
                int billNo = rs.getInt(1);
                int total = rs.getInt(2);
                int discount = rs.getInt(3);
                int grandTotal = rs.getInt(4);
                String dateTime = rs.getString(5);
                Vector vector = new Vector();
                vector.add(billNo);
                vector.add("Rs. " + total);
                vector.add("Rs. " + discount);
                vector.add("Rs. " + grandTotal);
                vector.add(dateTime);
                model.addRow(vector);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return model;
    }

    public List<BillingModel> getBillHistory(int billNo) {
        getConnection();
        List<BillingModel> billList = new ArrayList<>();
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from bill_history where billno=" + billNo);
            while (rs.next()) {
                String name = rs.getString(2);
                int qty = rs.getInt(3);
                int price = rs.getInt(4);
                int total = rs.getInt(5);
                BillingModel model = new BillingModel();
                model.setName(name);
                model.setQty(qty);
                model.setPrice(price);
                model.setTotal(total);
                billList.add(model);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return billList;
    }

    public String getTableNameByBillNo(int billNo) {
        getConnection();
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select table_id from bill_history where billno=" + billNo);
            rs.next();
            int table_no = rs.getInt(1);
            rs = stmt.executeQuery("select name from tables where id=" + table_no);
            rs.next();
            return rs.getString(1);
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Table Not Found";
    }

    public int getDiscountAmount(int billNo) {
        getConnection();
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select discount from final_bill where billno=" + billNo);
            rs.next();
            return rs.getInt(1);
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int getTodayIncome() {
        getConnection();
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select sum(grand_total) from final_bill where date_time like'%" + Utils.getDateToday() + "%'");
            rs.next();
            return rs.getInt(1);
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int getMonthlyIncome() {
        getConnection();
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select sum(grand_total) from final_bill where date_time between '" + Utils.getStartMonth() + "' and '" + Utils.getDateToday() + "'");
            rs.next();
            return rs.getInt(1);
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int getYearlyIncome() {
        getConnection();
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select sum(grand_total) from final_bill where date_time between '" + Utils.getStartYear() + "' and '" + Utils.getDateToday() + "'");
            rs.next();
            return rs.getInt(1);
        } catch (SQLException ex) {
            Logger.getLogger(DBHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
}
