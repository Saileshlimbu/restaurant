package panels;

import dbhandler.DBHandler;
import dialogs.SwitchTableDialog;
import global.MessageBox;
import global.Utils;
import java.awt.event.ActionEvent;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.PrinterJob;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import models.BillingModel;
import models.FinalBillModel;

public class BillingPanel extends javax.swing.JPanel {

    JPopupMenu popup;
    DBHandler db;
    int tableID;
    int discountPercentage = 0;
    List<BillingModel> billList;
    int billNo;

    public BillingPanel(int tableID) {
        initComponents();
        initiatePopupMenu();
        db = new DBHandler();
        this.tableID = tableID;
        lblTableName.setText(db.getTableName(tableID));
        billList = new ArrayList<>();
        if (db.hasAlreadyBill(tableID)) {
            billNo = db.getBillNo(tableID);
        } else {
            billNo = generateBillNo();
        }
        showOnTable(db.getBill(tableID));
        tblSearch.fixTable(jScrollPane3);
    }

    public void switchTable(int tableID) {
        if (db.switchTable(this.tableID, tableID)) {
            this.tableID = tableID;
            lblTableName.setText(db.getTableName(tableID));
        } else {
            MessageBox.showInfoMessage(this, "Error", "Table is already reserved.");
        }
    }

    private void initiatePopupMenu() {
        popup = new JPopupMenu();
        JMenuItem mnuEdit = new JMenuItem("Edit");
        mnuEdit.addActionListener((ActionEvent e) -> {
            String qty = JOptionPane.showInputDialog("Change your quantity.");
            if (Utils.isNumeric(qty)) {
                int row = tblBilling.getSelectedRow();
                int id = (int) tblBilling.getValueAt(row, 0);
                db.updateBillItem(id, Integer.parseInt(qty));
                showOnTable(db.getBill(tableID));
            } else {
                MessageBox.showInfoMessage(this, "Error", "Please enter a valid quantity.");
            }
        });

        JMenuItem mnuDelete = new JMenuItem("Delete");
        mnuDelete.addActionListener((ActionEvent e) -> {
            int answer = JOptionPane.showConfirmDialog(BillingPanel.this, "Are you sure?\nYou want to delete this item ?", "Select an option", JOptionPane.YES_NO_OPTION);
            if (answer == 0) {
                int row = tblBilling.getSelectedRow();
                if (db.deleteBillItem((int) tblBilling.getValueAt(row, 0))) {
                    showOnTable(db.getBill(tableID));
                    Utils.showSuccessMessage(BillingPanel.this, "Bill item has been deleted!!!");
                } else {
                    Utils.showErrorMessage(BillingPanel.this, "Failed to delete bill.");
                }
            }
        });
        popup.add(mnuEdit);
        popup.add(mnuDelete);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelGradient1 = new custom_ui.PanelGradient();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblBilling = new javax.swing.JTable();
        lblGrandTotal = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lblDiscount = new javax.swing.JLabel();
        etSearchBox = new swing.TextFieldAnimation();
        btnAddDiscount = new custom_ui.ButtonGradient();
        btnClear = new custom_ui.ButtonGradient();
        lblTableName = new javax.swing.JLabel();
        lblChangeTable = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblSearch = new custom_ui.Table();

        setBackground(new java.awt.Color(248, 248, 248));

        panelGradient1.setBackground(new java.awt.Color(248, 248, 248));
        panelGradient1.setColorPrimario(new java.awt.Color(248, 248, 248));
        panelGradient1.setColorSecundario(new java.awt.Color(248, 248, 248));

        tblBilling.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Food Name", "Qty", "Price", "Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblBilling.setGridColor(new java.awt.Color(255, 255, 255));
        tblBilling.setRowHeight(25);
        tblBilling.setSelectionBackground(new java.awt.Color(0, 51, 102));
        tblBilling.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblBillingMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblBilling);

        lblGrandTotal.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        lblGrandTotal.setForeground(new java.awt.Color(0, 102, 204));
        lblGrandTotal.setText("Rs 00.00");

        lblTotal.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        lblTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotal.setText("Rs 00.00");

        jLabel6.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 102, 255));
        jLabel6.setText("Grand Total");

        jLabel7.setFont(new java.awt.Font("Lucida Grande", 1, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 51, 102));
        jLabel7.setText("Billing");

        jLabel4.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel4.setText("Total");

        jLabel2.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 0, 0));
        jLabel2.setText("Discount");

        lblDiscount.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        lblDiscount.setForeground(new java.awt.Color(255, 0, 0));
        lblDiscount.setText("Rs. 00.00");

        etSearchBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                etSearchBoxKeyReleased(evt);
            }
        });

        btnAddDiscount.setText("Add Discount");
        btnAddDiscount.setColor1(new java.awt.Color(139, 199, 236));
        btnAddDiscount.setColor2(new java.awt.Color(55, 131, 210));
        btnAddDiscount.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnAddDiscountMousePressed(evt);
            }
        });
        btnAddDiscount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddDiscountActionPerformed(evt);
            }
        });

        btnClear.setText("Clear");
        btnClear.setColor1(new java.awt.Color(139, 199, 236));
        btnClear.setColor2(new java.awt.Color(55, 131, 210));
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        lblTableName.setFont(new java.awt.Font("Lucida Grande", 1, 12)); // NOI18N
        lblTableName.setForeground(new java.awt.Color(0, 51, 102));
        lblTableName.setText("Table 1");

        lblChangeTable.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/alter.png"))); // NOI18N
        lblChangeTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblChangeTableMousePressed(evt);
            }
        });

        jScrollPane3.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane3.setBorder(null);
        jScrollPane3.setForeground(new java.awt.Color(255, 255, 255));

        tblSearch.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Name", "Category", "Price"
            }
        ));
        tblSearch.setGridColor(new java.awt.Color(255, 255, 255));
        tblSearch.setOpaque(false);
        tblSearch.setRowHeight(50);
        tblSearch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblSearchMousePressed(evt);
            }
        });
        jScrollPane3.setViewportView(tblSearch);

        panelGradient1.setLayer(jScrollPane1, javax.swing.JLayeredPane.DEFAULT_LAYER);
        panelGradient1.setLayer(lblGrandTotal, javax.swing.JLayeredPane.DEFAULT_LAYER);
        panelGradient1.setLayer(lblTotal, javax.swing.JLayeredPane.DEFAULT_LAYER);
        panelGradient1.setLayer(jLabel6, javax.swing.JLayeredPane.DEFAULT_LAYER);
        panelGradient1.setLayer(jLabel7, javax.swing.JLayeredPane.DEFAULT_LAYER);
        panelGradient1.setLayer(jLabel4, javax.swing.JLayeredPane.DEFAULT_LAYER);
        panelGradient1.setLayer(jLabel2, javax.swing.JLayeredPane.DEFAULT_LAYER);
        panelGradient1.setLayer(lblDiscount, javax.swing.JLayeredPane.DEFAULT_LAYER);
        panelGradient1.setLayer(etSearchBox, javax.swing.JLayeredPane.DEFAULT_LAYER);
        panelGradient1.setLayer(btnAddDiscount, javax.swing.JLayeredPane.DEFAULT_LAYER);
        panelGradient1.setLayer(btnClear, javax.swing.JLayeredPane.DEFAULT_LAYER);
        panelGradient1.setLayer(lblTableName, javax.swing.JLayeredPane.DEFAULT_LAYER);
        panelGradient1.setLayer(lblChangeTable, javax.swing.JLayeredPane.DEFAULT_LAYER);
        panelGradient1.setLayer(jScrollPane3, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout panelGradient1Layout = new javax.swing.GroupLayout(panelGradient1);
        panelGradient1.setLayout(panelGradient1Layout);
        panelGradient1Layout.setHorizontalGroup(
            panelGradient1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGradient1Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(panelGradient1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelGradient1Layout.createSequentialGroup()
                        .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnAddDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(9, 9, 9))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelGradient1Layout.createSequentialGroup()
                        .addGroup(panelGradient1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(panelGradient1Layout.createSequentialGroup()
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblDiscount))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelGradient1Layout.createSequentialGroup()
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblGrandTotal))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelGradient1Layout.createSequentialGroup()
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(13, 13, 13))
                    .addGroup(panelGradient1Layout.createSequentialGroup()
                        .addComponent(lblTableName)
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(lblChangeTable)
                        .addGap(1, 1, 1))
                    .addComponent(jScrollPane1))
                .addGroup(panelGradient1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelGradient1Layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelGradient1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelGradient1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelGradient1Layout.createSequentialGroup()
                                .addComponent(jScrollPane3)
                                .addGap(2, 2, 2))
                            .addComponent(etSearchBox, javax.swing.GroupLayout.PREFERRED_SIZE, 454, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(31, 31, 31))
        );
        panelGradient1Layout.setVerticalGroup(
            panelGradient1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGradient1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(panelGradient1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblTableName)
                    .addComponent(jLabel7)
                    .addComponent(lblChangeTable))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelGradient1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelGradient1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 383, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelGradient1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(lblTotal))
                        .addGap(13, 13, 13)
                        .addGroup(panelGradient1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(14, 14, 14)
                        .addGroup(panelGradient1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addGroup(panelGradient1Layout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addComponent(lblGrandTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(28, 28, 28)
                        .addGroup(panelGradient1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnAddDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panelGradient1Layout.createSequentialGroup()
                        .addComponent(etSearchBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane3)))
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelGradient1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelGradient1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(30, 30, 30))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tblBillingMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblBillingMouseClicked
        if (SwingUtilities.isRightMouseButton(evt) == true) {
            popup.show(tblBilling, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_tblBillingMouseClicked

    private void btnAddDiscountMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddDiscountMousePressed
//        ArrayList<String> itemName = new ArrayList<>();
//        ArrayList<String> quantity = new ArrayList<>();
//        ArrayList<String> itemPrice = new ArrayList<>();
//        itemName.add("Mo:Mo");
//        itemName.add("Pizza");
//        itemName.add("Burger");
//        quantity.add("2");
//        quantity.add("3");
//        quantity.add("1");
//        itemPrice.add("200");
//        itemPrice.add("350");
//        itemPrice.add("100");
//        BillPrintable printable = new BillPrintable(this, itemName, quantity, itemPrice);
//        PrinterJob pj = PrinterJob.getPrinterJob();
//        pj.setPrintable(printable, getPageFormat(pj));
//        pj.printDialog();
//        try {
//            pj.print();
//        } catch (PrinterException ex) {
//            ex.printStackTrace();
//        }
    }//GEN-LAST:event_btnAddDiscountMousePressed

    private void etSearchBoxKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_etSearchBoxKeyReleased
        if (!etSearchBox.getText().equals("")) {
            tblSearch.setModel(db.getFoodList(etSearchBox.getText()));
        }else{
            tblSearch.setModel(db.getFoodList("aisdfaskdnw"));
        }
    }//GEN-LAST:event_etSearchBoxKeyReleased

    private int generateBillNo() {
        String time = String.valueOf(System.currentTimeMillis());
        return Integer.parseInt(time.substring(time.length() - 5));
    }

    private void btnAddDiscountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddDiscountActionPerformed
        String discount = JOptionPane.showInputDialog("Enter discount percentage(%).", 0);
        if (Utils.isNumeric(discount)) {
            discountPercentage = Integer.parseInt(discount);
            showOnTable(db.getBill(tableID));
        } else {
            MessageBox.showInfoMessage(this, "Error", "Please enter a valid discount.");
        }
    }//GEN-LAST:event_btnAddDiscountActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        int answer = JOptionPane.showConfirmDialog(BillingPanel.this, "Are you sure?\nYou want to clear this table items ?", "Select an option", JOptionPane.YES_NO_OPTION);
        if (answer == 0) {
            payBill(db.getBill(tableID));
            db.saveToHistory(db.getBill(tableID), billNo);
            billList.clear();
            showOnTable(db.getBill(tableID));
        }
    }//GEN-LAST:event_btnClearActionPerformed

    private void lblChangeTableMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblChangeTableMousePressed
        JFrame parent = (JFrame) this.getParent().getParent().getParent().getParent().getParent().getParent();
        SwitchTableDialog dialog = new SwitchTableDialog(parent, true, this);
        dialog.setLocationRelativeTo(parent);
        dialog.setVisible(true);
    }//GEN-LAST:event_lblChangeTableMousePressed

    private void tblSearchMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblSearchMousePressed
        int row = tblSearch.rowAtPoint(evt.getPoint());
        if (evt.getClickCount() == 2 && row != -1) {
            String qty = JOptionPane.showInputDialog("Enter Quantity ?");
            if(qty.equals(""))return;
            if (Utils.isNumeric(qty)) {
                int id = (int) tblSearch.getValueAt(row, 0);
                String name = (String) tblSearch.getValueAt(row, 1);
                String category = (String) tblSearch.getValueAt(row, 2);
                int price = (int) tblSearch.getValueAt(row, 3);
                BillingModel bill = new BillingModel();
                bill.setName(name);
                bill.setCategory(category);
                bill.setQty(Integer.parseInt(qty));
                bill.setPrice(price);
                bill.setTable_id(tableID);
                db.addBill(bill, billNo);
                showOnTable(db.getBill(tableID));
            } else {
                MessageBox.showInfoMessage(this, "Error", "Please enter a valid quantity.");
            }
        }
    }//GEN-LAST:event_tblSearchMousePressed

    public PageFormat getPageFormat(PrinterJob pj) {

        PageFormat pf = pj.defaultPage();
        Paper paper = pf.getPaper();

        double bodyHeight = 0.0;
        double headerHeight = 5.0;
        double footerHeight = 5.0;
        double width = cm_to_pp(8);
        double height = cm_to_pp(headerHeight + bodyHeight + footerHeight);
        paper.setSize(width, height);
        paper.setImageableArea(0, 10, width, height - cm_to_pp(1));

        pf.setOrientation(PageFormat.PORTRAIT);
        pf.setPaper(paper);

        return pf;
    }

    protected static double cm_to_pp(double cm) {
        return toPPI(cm * 0.393600787);
    }

    protected static double toPPI(double inch) {
        return inch * 72d;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private custom_ui.ButtonGradient btnAddDiscount;
    private custom_ui.ButtonGradient btnClear;
    private swing.TextFieldAnimation etSearchBox;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblChangeTable;
    private javax.swing.JLabel lblDiscount;
    private javax.swing.JLabel lblGrandTotal;
    private javax.swing.JLabel lblTableName;
    private javax.swing.JLabel lblTotal;
    private custom_ui.PanelGradient panelGradient1;
    private javax.swing.JTable tblBilling;
    private custom_ui.Table tblSearch;
    // End of variables declaration//GEN-END:variables

    private void showOnTable(List<BillingModel> billList) {
        String[] column = new String[]{"ID", "Food Name", "Qty", "Price", "Total"};
        int total = 0;
        DefaultTableModel model = new DefaultTableModel(column, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }

        };
        for (BillingModel bill : billList) {
            Vector vector = new Vector();
            vector.add(bill.getId());
            vector.add(bill.getName());
            vector.add(bill.getQty());
            vector.add(bill.getPrice());
            int tempTotal = bill.getQty() * bill.getPrice();
            vector.add(tempTotal);
            model.addRow(vector);
            total += tempTotal;
        }
        tblBilling.setModel(model);
        lblTotal.setText("Rs." + total);
        int discountAmount = (total * discountPercentage / 100);
        lblDiscount.setText("Rs." + discountAmount);
        lblGrandTotal.setText("Rs." + (total - discountAmount));
    }

    private void payBill(List<BillingModel> billList) {
        int total = 0;
        for (BillingModel bill : billList) {
            int tempTotal = bill.getQty() * bill.getPrice();
            total += tempTotal;
        }
        int discountAmount = (total * discountPercentage / 100);
        FinalBillModel model = new FinalBillModel();
        model.setTotal(total);
        model.setDiscount(discountAmount);
        model.setGrandTotal(total - discountAmount);
        model.setBillNo(billNo);
        model.setDateTime(Utils.getDate());
        db.payBill(model);
    }
}
