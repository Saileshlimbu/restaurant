/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panels;

import dbhandler.DBHandler;
import global.Utils;
import java.awt.event.ActionEvent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import models.FoodModel;

/**
 *
 * @author samitalimbu
 */
public class FoodPanel extends javax.swing.JPanel {

    JPopupMenu popup;
    DBHandler db;
    int editID;

    public FoodPanel() {
        initComponents();
        initiatePopupMenu();
        db = new DBHandler();
        tblFood.fixTable(jScrollPane2);
        tblFood.setModel(db.getFoodList());
    }

    private void initiatePopupMenu() {
        popup = new JPopupMenu();
        JMenuItem mnuEdit = new JMenuItem("Edit");
        mnuEdit.addActionListener((ActionEvent e) -> {
            int row = tblFood.getSelectedRow();
            editID = (int) tblFood.getValueAt(row, 0);
            String name = (String) tblFood.getValueAt(row, 1);
            String category = (String) tblFood.getValueAt(row, 2);
            int price = (int) tblFood.getValueAt(row, 3);

            etFoodName.setText(name);
            cmbCategory.setSelectedItem(category);
            etPrice.setText(Integer.toString(price));
            btnAddFood.setText("Update Food");
        });

        JMenuItem mnuDelete = new JMenuItem("Delete");
        mnuDelete.addActionListener((ActionEvent e) -> {
            int row = tblFood.getSelectedRow();
            int id = (int) tblFood.getValueAt(row, 0);

            if (db.deleteFood(id)) {
                Utils.showSuccessMessage(this, "Food deleted successfully!!!");
                tblFood.setModel(db.getFoodList());
            } else {
                Utils.showErrorMessage(this, "Failed to delete food.");
            }

        });
        popup.add(mnuEdit);
        popup.add(mnuDelete);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        textFieldAnimation1 = new swing.TextFieldAnimation();
        jPanel1 = new javax.swing.JPanel();
        panelBorder1 = new custom_ui.PanelBorder();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        etFoodName = new custom_ui.MyTextField();
        jLabel3 = new javax.swing.JLabel();
        cmbCategory = new javax.swing.JComboBox<>();
        btnAddFood = new custom_ui.ButtonGradient();
        jLabel4 = new javax.swing.JLabel();
        etPrice = new custom_ui.MyTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblFood = new custom_ui.Table();

        setBackground(new java.awt.Color(248, 248, 248));

        jPanel1.setBackground(new java.awt.Color(248, 248, 248));
        jPanel1.setLayout(new java.awt.GridBagLayout());

        panelBorder1.setBackground(new java.awt.Color(255, 255, 255));
        panelBorder1.setRadius(20);

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 153));
        jLabel1.setText("Add Food");

        jLabel2.setText("Food Name");

        jLabel3.setText("Category");

        cmbCategory.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbCategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbCategoryActionPerformed(evt);
            }
        });

        btnAddFood.setText("Add Food");
        btnAddFood.setColor1(new java.awt.Color(139, 199, 236));
        btnAddFood.setColor2(new java.awt.Color(55, 131, 210));
        btnAddFood.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddFoodActionPerformed(evt);
            }
        });

        jLabel4.setText("Price");

        javax.swing.GroupLayout panelBorder1Layout = new javax.swing.GroupLayout(panelBorder1);
        panelBorder1.setLayout(panelBorder1Layout);
        panelBorder1Layout.setHorizontalGroup(
            panelBorder1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBorder1Layout.createSequentialGroup()
                .addGroup(panelBorder1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelBorder1Layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addGroup(panelBorder1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(etFoodName, javax.swing.GroupLayout.DEFAULT_SIZE, 289, Short.MAX_VALUE)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(etPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbCategory, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(panelBorder1Layout.createSequentialGroup()
                        .addGap(102, 102, 102)
                        .addComponent(jLabel1))
                    .addGroup(panelBorder1Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(btnAddFood, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelBorder1Layout.setVerticalGroup(
            panelBorder1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBorder1Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(etFoodName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmbCategory, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(etPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24)
                .addComponent(btnAddFood, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(31, Short.MAX_VALUE))
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipady = 20;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(110, 68, 125, 69);
        jPanel1.add(panelBorder1, gridBagConstraints);

        tblFood.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Food Name", "Category", "Price"
            }
        ));
        tblFood.setRowHeight(50);
        tblFood.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblFoodMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblFood);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textFieldAnimation1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 442, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(textFieldAnimation1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 571, Short.MAX_VALUE))
                .addGap(30, 30, 30))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void cmbCategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbCategoryActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbCategoryActionPerformed

    private void btnAddFoodActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddFoodActionPerformed
        if (!validateFood()) {
            return;
        }
        String foodName = etFoodName.getText();
        String category = cmbCategory.getSelectedItem().toString();
        int price = Integer.parseInt(etPrice.getText());
        FoodModel food = new FoodModel(foodName, category, price);
        food.setId(editID);
        if (btnAddFood.getText().equals("Update Food")) {
            if (db.updateFoodItem(food)) {
                tblFood.setModel(db.getFoodList());
                Utils.showSuccessMessage(this, "Food item updated successfully!!!");
                etFoodName.setText("");
                etPrice.setText("");
                cmbCategory.setSelectedIndex(0);
                btnAddFood.setText("Add Food");
            } else {
                Utils.showErrorMessage(this, "Failed to update food item.");
            }
        } else {
            if (db.addFoodItem(food)) {
                tblFood.setModel(db.getFoodList());
                Utils.showSuccessMessage(this, "Food item added successfully!!!");
                etFoodName.setText("");
                etPrice.setText("");
                cmbCategory.setSelectedIndex(0);
            } else {
                Utils.showErrorMessage(this, "Failed to add food item.");
            }
        }
    }//GEN-LAST:event_btnAddFoodActionPerformed

    private void tblFoodMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblFoodMouseClicked
        if (SwingUtilities.isRightMouseButton(evt) == true) {
            JTable source = (JTable) evt.getSource();
            int row = source.rowAtPoint(evt.getPoint());
            int column = source.columnAtPoint(evt.getPoint());

            if (!source.isRowSelected(row)) {
                source.changeSelection(row, column, false, false);
            }

            popup.show(evt.getComponent(), evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_tblFoodMouseClicked

    private boolean validateFood() {
        if (etFoodName.getText().equals("")) {
            Utils.showInfoMessage(this, "Food name is required!");
            return false;
        } else if (etPrice.getText().equals("")) {
            Utils.showInfoMessage(this, "Price is required!");
            return false;
        } else if (!Utils.isNumeric(etPrice.getText())) {
            Utils.showInfoMessage(this, "Enter a valid price.");
            return false;
        }
        return true;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private custom_ui.ButtonGradient btnAddFood;
    private javax.swing.JComboBox<String> cmbCategory;
    private custom_ui.MyTextField etFoodName;
    private custom_ui.MyTextField etPrice;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private custom_ui.PanelBorder panelBorder1;
    private custom_ui.Table tblFood;
    private swing.TextFieldAnimation textFieldAnimation1;
    // End of variables declaration//GEN-END:variables
}
