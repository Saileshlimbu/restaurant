package panels;

import dialogs.HistoryDialog;
import dbhandler.DBHandler;
import java.awt.Point;
import javax.swing.JFrame;
import javax.swing.JTable;

public class HistoryPanel extends javax.swing.JPanel {

    DBHandler db;

    public HistoryPanel() {
        initComponents();
        db = new DBHandler();
        tblHistory.fixTable(jScrollPane2);
        tblHistory.setModel(db.getFinalBillList());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        etSearchBox = new swing.TextFieldAnimation();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblHistory = new custom_ui.Table();

        setBackground(new java.awt.Color(248, 248, 248));

        etSearchBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                etSearchBoxKeyReleased(evt);
            }
        });

        tblHistory.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Bill No", "Total", "Discount", "Grand Total", "Date Time"
            }
        ));
        tblHistory.setRowHeight(50);
        tblHistory.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblHistoryMousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(tblHistory);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 644, Short.MAX_VALUE)
                    .addComponent(etSearchBox, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(30, 30, 30))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(etSearchBox, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 357, Short.MAX_VALUE)
                .addGap(30, 30, 30))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void etSearchBoxKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_etSearchBoxKeyReleased
        if (!etSearchBox.getText().equals("")) {
            tblHistory.setModel(db.searchFinalBill(etSearchBox.getText()));
        }
    }//GEN-LAST:event_etSearchBoxKeyReleased

    private void tblHistoryMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblHistoryMousePressed
       JTable table = (JTable) evt.getSource();
        Point point = evt.getPoint();
        int row = table.rowAtPoint(point);
        if (evt.getClickCount() == 2 && table.getSelectedRow() != -1) {
            int row1 = tblHistory.getSelectedRow();
            int billNo = (int) tblHistory.getValueAt(row1, 0);
            JFrame parent = (JFrame)this.getParent().getParent().getParent().getParent().getParent();
            HistoryDialog dialog = new HistoryDialog(parent, true, billNo);
            dialog.setLocationRelativeTo(parent);
            dialog.setTitle("Billing History");
            dialog.setVisible(true);
        }
    }//GEN-LAST:event_tblHistoryMousePressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private swing.TextFieldAnimation etSearchBox;
    private javax.swing.JScrollPane jScrollPane2;
    private custom_ui.Table tblHistory;
    // End of variables declaration//GEN-END:variables
}
