
package panels;

import java.awt.event.ActionEvent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;


public class Invoicebill extends javax.swing.JPanel {
JPopupMenu popup;

    private void initiatePopupMenu() {
        popup = new JPopupMenu();
        JMenuItem mnuEdit = new JMenuItem("Edit");
        mnuEdit.addActionListener((ActionEvent e) -> {
        });

        JMenuItem mnuDelete = new JMenuItem("Delete");
        mnuDelete.addActionListener((ActionEvent e) -> {
        });
        popup.add(mnuEdit);
        popup.add(mnuDelete);
    }

    
    public Invoicebill() {
        initComponents();
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jLabel40 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel50 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(null);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/logorestaurant.png"))); // NOI18N
        add(jLabel1);
        jLabel1.setBounds(29, 17, 100, 82);

        jLabel2.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel2.setText("Nagar Jun-5, Swyambhu, Chhakdol");
        add(jLabel2);
        jLabel2.setBounds(234, 17, 185, 16);

        jLabel4.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/facebook-3.png"))); // NOI18N
        jLabel4.setText("www.facebook.com/imaginerestro");
        add(jLabel4);
        jLabel4.setBounds(234, 59, 203, 16);
        add(jLabel5);
        jLabel5.setBounds(216, 112, 0, 0);

        jLabel6.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/smartphone.png"))); // NOI18N
        jLabel6.setText("+ 977-9802057511");
        add(jLabel6);
        jLabel6.setBounds(234, 37, 129, 16);

        jLabel3.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/instagram-2.png"))); // NOI18N
        jLabel3.setText(" www.instagram.com/imaginerestro");
        add(jLabel3);
        jLabel3.setBounds(234, 81, 211, 16);
        add(jLabel7);
        jLabel7.setBounds(452, 110, 0, 0);

        jLabel8.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/gmail.png"))); // NOI18N
        jLabel8.setText("imaginerestro@gmail.com");
        add(jLabel8);
        jLabel8.setBounds(234, 103, 159, 16);

        jLabel9.setBackground(new java.awt.Color(51, 51, 51));
        jLabel9.setForeground(new java.awt.Color(51, 51, 51));
        jLabel9.setText("jLabel9");
        jLabel9.setOpaque(true);
        add(jLabel9);
        jLabel9.setBounds(6, 124, 502, 1);

        jLabel10.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel10.setText("Transaction Date: 2077/05/01");
        add(jLabel10);
        jLabel10.setBounds(6, 133, 165, 14);

        jLabel11.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel11.setText("Transaction Time: 12;35:22 PM");
        add(jLabel11);
        jLabel11.setBounds(6, 148, 165, 14);

        jLabel12.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel12.setText("Bill To: Lin ye");
        add(jLabel12);
        jLabel12.setBounds(6, 163, 69, 14);

        jLabel13.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel13.setText("Mobile No: 9865347489");
        add(jLabel13);
        jLabel13.setBounds(6, 178, 129, 14);
        add(jLabel14);
        jLabel14.setBounds(508, 237, 0, 0);

        jLabel15.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel15.setText("Payment Mode: Cash");
        add(jLabel15);
        jLabel15.setBounds(6, 193, 110, 14);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        add(jPanel1);
        jPanel1.setBounds(20, 237, 15, 0);

        jTable1.setAutoCreateColumnsFromModel(false);
        jTable1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "S.N", "Name", "Category", "Price", "Discount"
            }
        ));
        jTable1.setOpaque(false);
        jTable1.setSelectionBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setViewportView(jTable1);

        add(jScrollPane1);
        jScrollPane1.setBounds(6, 219, 500, 242);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(null);

        jLabel25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/logorestaurant.png"))); // NOI18N
        jPanel2.add(jLabel25);
        jLabel25.setBounds(29, 17, 100, 82);

        jLabel26.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel26.setText("Nagar Jun-5, Swyambhu, Chhakdol");
        jPanel2.add(jLabel26);
        jLabel26.setBounds(216, 17, 185, 16);

        jLabel27.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/facebook-3.png"))); // NOI18N
        jLabel27.setText("www.facebook.com/imaginerestro");
        jPanel2.add(jLabel27);
        jLabel27.setBounds(216, 59, 203, 16);
        jPanel2.add(jLabel28);
        jLabel28.setBounds(198, 112, 0, 0);

        jLabel29.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/smartphone.png"))); // NOI18N
        jLabel29.setText("+ 977-9802057511");
        jPanel2.add(jLabel29);
        jLabel29.setBounds(216, 37, 129, 16);

        jLabel30.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/instagram-2.png"))); // NOI18N
        jLabel30.setText(" www.instagram.com/imaginerestro");
        jPanel2.add(jLabel30);
        jLabel30.setBounds(216, 81, 211, 16);
        jPanel2.add(jLabel31);
        jLabel31.setBounds(434, 110, 0, 0);

        jLabel32.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/gmail.png"))); // NOI18N
        jLabel32.setText("imaginerestro@gmail.com");
        jPanel2.add(jLabel32);
        jLabel32.setBounds(216, 103, 159, 16);

        jLabel33.setBackground(new java.awt.Color(51, 51, 51));
        jLabel33.setForeground(new java.awt.Color(51, 51, 51));
        jLabel33.setText("jLabel9");
        jLabel33.setOpaque(true);
        jPanel2.add(jLabel33);
        jLabel33.setBounds(6, 124, 484, 1);

        jLabel34.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel34.setText("Transaction Date: 2077/05/01");
        jPanel2.add(jLabel34);
        jLabel34.setBounds(6, 133, 165, 14);

        jLabel35.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel35.setText("Transaction Time: 12;35:22 PM");
        jPanel2.add(jLabel35);
        jLabel35.setBounds(6, 148, 165, 14);

        jLabel36.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel36.setText("Bill To: Lin ye");
        jPanel2.add(jLabel36);
        jLabel36.setBounds(6, 163, 69, 14);

        jLabel37.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel37.setText("Mobile No: 9865347489");
        jPanel2.add(jLabel37);
        jLabel37.setBounds(6, 178, 129, 14);
        jPanel2.add(jLabel38);
        jLabel38.setBounds(490, 237, 0, 0);

        jLabel39.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel39.setText("Payment Mode: Cash");
        jPanel2.add(jLabel39);
        jLabel39.setBounds(6, 193, 110, 14);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel2.add(jPanel3);
        jPanel3.setBounds(6, 273, 0, 0);

        jTable2.setAutoCreateColumnsFromModel(false);
        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "S.N", "Name", "Category", "Price", "Discount"
            }
        ));
        jScrollPane2.setViewportView(jTable2);

        jPanel2.add(jScrollPane2);
        jScrollPane2.setBounds(12, 219, 472, 242);

        jLabel40.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel40.setText("Discount:");
        jPanel2.add(jLabel40);
        jLabel40.setBounds(323, 491, 51, 14);

        jLabel41.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel41.setText("Sub Total:  ");
        jPanel2.add(jLabel41);
        jLabel41.setBounds(323, 471, 59, 14);

        jLabel42.setBackground(new java.awt.Color(51, 51, 51));
        jLabel42.setForeground(new java.awt.Color(51, 51, 51));
        jLabel42.setText("jLabel9");
        jLabel42.setOpaque(true);
        jPanel2.add(jLabel42);
        jLabel42.setBounds(200, 510, 290, 1);

        jLabel43.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel43.setText(" Net Amount: ");
        jPanel2.add(jLabel43);
        jLabel43.setBounds(323, 523, 73, 14);

        jLabel44.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel44.setText("Cash:");
        jPanel2.add(jLabel44);
        jLabel44.setBounds(323, 543, 30, 14);

        jLabel45.setBackground(new java.awt.Color(51, 51, 51));
        jLabel45.setForeground(new java.awt.Color(51, 51, 51));
        jLabel45.setText("jLabel9");
        jLabel45.setOpaque(true);
        jPanel2.add(jLabel45);
        jLabel45.setBounds(198, 563, 292, 1);

        jLabel46.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel46.setText("Amount Due:");
        jPanel2.add(jLabel46);
        jLabel46.setBounds(323, 569, 70, 14);

        jLabel47.setBackground(new java.awt.Color(51, 51, 51));
        jLabel47.setForeground(new java.awt.Color(51, 51, 51));
        jLabel47.setText("jLabel9");
        jLabel47.setOpaque(true);
        jPanel2.add(jLabel47);
        jLabel47.setBounds(6, 591, 484, 1);

        jLabel48.setText("***********************Thank You**********************");
        jPanel2.add(jLabel48);
        jLabel48.setBounds(56, 601, 337, 16);

        jLabel49.setBackground(new java.awt.Color(51, 51, 51));
        jLabel49.setForeground(new java.awt.Color(51, 51, 51));
        jLabel49.setText("jLabel9");
        jLabel49.setOpaque(true);
        jPanel2.add(jLabel49);
        jLabel49.setBounds(6, 635, 484, 1);

        add(jPanel2);
        jPanel2.setBounds(256, 320, 496, 653);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jLabel16.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel16.setText("Sub Total:  ");

        jLabel17.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel17.setText("Discount:");

        jLabel18.setBackground(new java.awt.Color(51, 51, 51));
        jLabel18.setForeground(new java.awt.Color(51, 51, 51));
        jLabel18.setText(".");
        jLabel18.setMaximumSize(new java.awt.Dimension(2, 16));
        jLabel18.setMinimumSize(new java.awt.Dimension(2, 16));
        jLabel18.setOpaque(true);

        jLabel19.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel19.setText(" Net Amount: ");

        jLabel20.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel20.setText("Cash:");

        jLabel21.setBackground(new java.awt.Color(51, 51, 51));
        jLabel21.setForeground(new java.awt.Color(51, 51, 51));
        jLabel21.setText("jLabel9");
        jLabel21.setOpaque(true);

        jLabel22.setFont(new java.awt.Font("Lucida Grande", 0, 11)); // NOI18N
        jLabel22.setText("Amount Due:");

        jLabel23.setBackground(new java.awt.Color(51, 51, 51));
        jLabel23.setForeground(new java.awt.Color(51, 51, 51));
        jLabel23.setText("jLabel9");
        jLabel23.setOpaque(true);

        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel24.setText("***********************Thank You**********************");

        jLabel50.setBackground(new java.awt.Color(51, 51, 51));
        jLabel50.setForeground(new java.awt.Color(51, 51, 51));
        jLabel50.setText("jLabel50");
        jLabel50.setOpaque(true);

        jLabel51.setBackground(new java.awt.Color(51, 51, 51));
        jLabel51.setForeground(new java.awt.Color(51, 51, 51));
        jLabel51.setText("jLabel51");
        jLabel51.setOpaque(true);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel23, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel50, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(305, 305, 305)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel16)
                            .addComponent(jLabel22)
                            .addComponent(jLabel17)
                            .addComponent(jLabel19)
                            .addComponent(jLabel20))
                        .addGap(0, 122, Short.MAX_VALUE))
                    .addComponent(jLabel51, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jLabel51, javax.swing.GroupLayout.PREFERRED_SIZE, 1, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 1, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel20)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 1, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel22)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 1, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel24)
                .addGap(18, 18, 18)
                .addComponent(jLabel50, javax.swing.GroupLayout.PREFERRED_SIZE, 1, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        add(jPanel4);
        jPanel4.setBounds(6, 467, 500, 187);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    // End of variables declaration//GEN-END:variables
}
