package panels;

import dbhandler.DBHandler;
import global.Utils;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import models.TableModel;

public class TablemanagementPanel extends javax.swing.JPanel {

    JPopupMenu popup;
    DBHandler db;
    int editID;
    int selectedRow;

    public TablemanagementPanel() {
        initComponents();
        initiatePopupMenu();
        db = new DBHandler();
        listTable.setModel(db.getTableNameList());
    }

    private void initiatePopupMenu() {
        popup = new JPopupMenu();
        JMenuItem mnuEdit = new JMenuItem("Edit");
        mnuEdit.addActionListener((ActionEvent e) -> {
            TableModel model = ((TableModel) db.getTableList().get(selectedRow));
            String tableName = model.getName();
            editID = model.getId();
            etTableName.setText(tableName);
            btnAddTable.setText("Update Table");
        });

        JMenuItem mnuDelete = new JMenuItem("Delete");
        mnuDelete.addActionListener((ActionEvent e) -> {
            TableModel model = ((TableModel) db.getTableList().get(selectedRow));
            if (db.deleteTable(model.getId())) {
                Utils.showSuccessMessage(this, "Table deleted successfully!!!");
                listTable.setModel(db.getTableNameList());
            } else {
                Utils.showErrorMessage(this, "Failed to delete table.");
            }
        });
        popup.add(mnuEdit);
        popup.add(mnuDelete);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jScrollPane2 = new javax.swing.JScrollPane();
        listTable = new javax.swing.JList<>();
        jPanel1 = new javax.swing.JPanel();
        panelBorder1 = new custom_ui.PanelBorder();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        etTableName = new custom_ui.MyTextField();
        btnAddTable = new custom_ui.ButtonGradient();
        jLabel3 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(248, 248, 248));

        listTable.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        listTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                listTableMousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(listTable);

        jPanel1.setBackground(new java.awt.Color(248, 248, 248));
        jPanel1.setLayout(new java.awt.GridBagLayout());

        panelBorder1.setBackground(new java.awt.Color(255, 255, 255));
        panelBorder1.setRadius(20);

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 153));
        jLabel1.setText("Add Table");

        jLabel2.setText("Enter Table Name");

        btnAddTable.setText("Add Table");
        btnAddTable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddTableActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelBorder1Layout = new javax.swing.GroupLayout(panelBorder1);
        panelBorder1.setLayout(panelBorder1Layout);
        panelBorder1Layout.setHorizontalGroup(
            panelBorder1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBorder1Layout.createSequentialGroup()
                .addGroup(panelBorder1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelBorder1Layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addGroup(panelBorder1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel2)
                            .addComponent(btnAddTable, javax.swing.GroupLayout.DEFAULT_SIZE, 289, Short.MAX_VALUE)
                            .addComponent(etTableName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(panelBorder1Layout.createSequentialGroup()
                        .addGap(102, 102, 102)
                        .addComponent(jLabel1)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelBorder1Layout.setVerticalGroup(
            panelBorder1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBorder1Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(etTableName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(btnAddTable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 10;
        gridBagConstraints.ipady = 44;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(98, 16, 99, 26);
        jPanel1.add(panelBorder1, gridBagConstraints);

        jLabel3.setBackground(new java.awt.Color(255, 153, 51));
        jLabel3.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(51, 0, 51));
        jLabel3.setText("Table List");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 423, Short.MAX_VALUE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 373, Short.MAX_VALUE)
                .addGap(30, 30, 30))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 501, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2)))
                .addGap(30, 30, 30))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void listTableMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listTableMousePressed
        if (SwingUtilities.isRightMouseButton(evt) == true) {
            JList source = (JList) evt.getSource();
            selectedRow = source.getSelectedIndex();

            if (selectedRow > -1) {
                popup.show(evt.getComponent(), evt.getX(), evt.getY());
            }
        }
    }//GEN-LAST:event_listTableMousePressed

    private void btnAddTableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddTableActionPerformed
        if (etTableName.getText().equals("")) {
            Utils.showInfoMessage(this, "Please enter table name.");
            return;
        }
        if (btnAddTable.getText().equals("Update Table")) {
            if (db.updateTable(new TableModel(editID, etTableName.getText()))) {
                Utils.showSuccessMessage(this, "Table updated successfully!!!");
                listTable.setModel(db.getTableNameList());
            } else {
                Utils.showErrorMessage(this, "Failed to update table.");
            }
        } else {
            if (db.addTable(new TableModel(etTableName.getText()))) {
                Utils.showSuccessMessage(this, "Table added successfully!!!");
                listTable.setModel(db.getTableNameList());
            } else {
                Utils.showErrorMessage(this, "Failed to add table.");
            }
        }
        etTableName.setText("");
        btnAddTable.setText("Add Table");
    }//GEN-LAST:event_btnAddTableActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private custom_ui.ButtonGradient btnAddTable;
    private custom_ui.MyTextField etTableName;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JList<String> listTable;
    private custom_ui.PanelBorder panelBorder1;
    // End of variables declaration//GEN-END:variables

}
