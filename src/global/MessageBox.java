/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package global;

import java.awt.Component;
import javax.swing.JOptionPane;

/**
 *
 * @author samitalimbu
 */
public class MessageBox {
    
    public static void showInfoMessage(Component component, String title, String message ){
        JOptionPane.showMessageDialog(component, message, title, JOptionPane.PLAIN_MESSAGE);
    }
    
}
