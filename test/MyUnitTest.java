
import dbhandler.DBHandler;
import models.FoodModel;
import models.TableModel;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class MyUnitTest {

    DBHandler db;

    @Before
    public void setUp() {
        db = new DBHandler();
    }

    @Test
    public void loginTest() {
        boolean expected = true;
        boolean actual = db.loginUser("samitalimbu", "123456");
        assertEquals(expected, actual);
    }

    @Test
    public void registerTest() {
        boolean expected = true;
        //This test fails because username with samitalimbu is already registered.
        boolean actual = db.registerUser("samitalimbu", "123456", "samitalimbu396@gmail.com");
        assertEquals(expected, actual);
    }

    @Test
    public void addFoodItem() {
        boolean expected = true;
        FoodModel food = new FoodModel("Salad", "Snacks", 100);
        boolean actual = db.addFoodItem(food);
        assertEquals(expected, actual);
    }

    @Test
    public void updateFoodItem() {
        boolean expected = true;
        FoodModel food = new FoodModel(10, "Salad", "Snacks", 100);
        boolean actual = db.updateFoodItem(food);
        assertEquals(expected, actual);
    }

    @Test
    public void deleteFoodItem() {
        boolean expected = true;
        //This test fails because we don't have food with id 9999
        boolean actual = db.deleteFood(9999);
        assertEquals(expected, actual);
    }

    @Test
    public void addTable() {
        boolean expected = true;
        TableModel model = new TableModel("Table 6");
        boolean actual = db.addTable(model);
        assertEquals(expected, actual);
    }

    @Test
    public void updateTable() {
        boolean expected = true;
        TableModel model = new TableModel(1, "Table 6");
        boolean actual = db.updateTable(model);
        assertEquals(expected, actual);
    }

    @Test
    public void deleteTable() {
        boolean expected = true;
        boolean actual = db.deleteTable(1);
        assertEquals(expected, actual);
    }

    @Test
    public void deleteBill() {
        boolean expected = true;
        //This test will likely to fail because we may not have bill with id(5)
        boolean actual = db.deleteBillItem(5);
        assertEquals(expected, actual);
    }

    @Test
    public void checkIfTableIsReserved() {
        boolean expected = true;
        boolean actual = db.isTableEmpty(5);
        assertEquals(expected, actual);
    }
}
